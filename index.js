const showManager = require('./src/show.js');
const removeManager = require('./src/delete.js');
const createManager = require('./src/create.js');
const AWS = require('aws-sdk');

exports.handler = async (event) => {
    // TODO implement

    switch (event.httpMethod) {
        case 'GET':
            return(showManager.show(event));
        case 'DELETE':
            return(removeManager.delete(event));
        case 'POST':
            return(createManager.create(JSON.parse(event.body)));
        default:
            // code
    }
};

