const AWS = require("aws-sdk");

const dynamo = new AWS.DynamoDB.DocumentClient();

exports.show = async (event) => {
    let body =  await dynamo.get({ TableName: "students" ,Key:{email:event['queryStringParameters']['id']}}).promise();
    const response = {
        statusCode: 200,
        body: JSON.stringify(body.Item),
    };
    return response;
     
};