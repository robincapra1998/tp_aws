const AWS = require("aws-sdk");

const dynamo = new AWS.DynamoDB.DocumentClient();


exports.create = async (requestBody) => {
     const params = {
        TableName:"students",
        Item: requestBody
    };
    return await dynamo.put(params).promise().then(() =>{
        const body = {
            Operation: 'SAVE',
            Message:'SUCCESS',
            Item: requestBody
        };
        return {
        statusCode: 200,
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify(body)
    };
    }, (error)=>{
        console.error("ERROR",error);
    });
};
