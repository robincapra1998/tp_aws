const AWS = require("aws-sdk");

const dynamo = new AWS.DynamoDB.DocumentClient();

exports.delete = async (event) => {
    let body = await dynamo.delete({ TableName: "students" ,Key:{email:event['queryStringParameters']['id']}}).promise();
        const response = {
        statusCode: 200,
        body: JSON.stringify(body.Item),
    };
    return response;
};